## Тестовое задание
Находится в файле [test-paper.docx](/test-paper.docx)
***

### Запуск проекта

```shell
cp .env.test .env
docker-compose up --build -d

cd app
cp .env.test .env

make composer-install
```

### Миграции и наполнение
```shell
make migrations
make fixtures
```

### Авто-тесты
```shell
make tests
```

### Прохождение тестов
http://127.0.0.1:8081/


