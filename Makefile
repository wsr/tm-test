composer-install:
	docker exec -it tm-php-cli composer install

migrations:
	docker exec -it tm-php-cli php ./bin/console doctrine:migrations:migrate

fixtures:
	docker exec -it tm-php-cli php ./bin/console doctrine:fixtures:load

tests:
	docker exec -it tm-php-cli php ./bin/phpunit
