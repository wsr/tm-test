<?php

namespace App\Core;

interface TestResultInterface extends IdentityInterface
{
    /**
     * @param UserInterface $user
     *
     * @return self
     */
    public function setUser(UserInterface $user): self;
}