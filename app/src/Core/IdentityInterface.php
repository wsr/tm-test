<?php

namespace App\Core;

interface IdentityInterface
{
    /**
     * @return string
     */
    public function getIdentity(): string;
}