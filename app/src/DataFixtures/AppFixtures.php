<?php

namespace App\DataFixtures;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Test;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $test = (new Test())->setTitle('TextMagic test');
        $manager->persist($test);

        $question1 = (new Question())->setText('1 + 1 = ');
        $answer11 = (new Answer())
            ->setText('3')
            ->setIsCorrect(false)
            ->setQuestion($question1);
        $answer12 = (new Answer())
            ->setText('2')
            ->setIsCorrect(true)
            ->setQuestion($question1);
        $answer13 = (new Answer())
            ->setText('0')
            ->setIsCorrect(false)
            ->setQuestion($question1);
        $manager->persist($question1);
        $manager->persist($answer11);
        $manager->persist($answer12);
        $manager->persist($answer13);
        $test->addQuestion($question1);

        $question2 = (new Question())->setText('2 + 2 = ');
        $answer21 = (new Answer())
            ->setText('4')
            ->setIsCorrect(true)
            ->setQuestion($question2);
        $answer22 = (new Answer())
            ->setText('3 + 1')
            ->setIsCorrect(true)
            ->setQuestion($question2);
        $answer23 = (new Answer())
            ->setText('10')
            ->setIsCorrect(false)
            ->setQuestion($question2);
        $manager->persist($question2);
        $manager->persist($answer21);
        $manager->persist($answer22);
        $manager->persist($answer23);
        $test->addQuestion($question2);

        $question3 = (new Question())->setText('3 + 3 = ');
        $answer31 = (new Answer())
            ->setText('1 + 5')
            ->setIsCorrect(true)
            ->setQuestion($question3);
        $answer32 = (new Answer())
            ->setText('1')
            ->setIsCorrect(false)
            ->setQuestion($question3);
        $answer33 = (new Answer())
            ->setText('6')
            ->setIsCorrect(true)
            ->setQuestion($question3);
        $answer34 = (new Answer())
            ->setText('2 + 4')
            ->setIsCorrect(true)
            ->setQuestion($question3);
        $manager->persist($question3);
        $manager->persist($answer31);
        $manager->persist($answer32);
        $manager->persist($answer33);
        $manager->persist($answer34);
        $test->addQuestion($question3);

        $question4 = (new Question())->setText('4 + 4 = ');
        $answer41 = (new Answer())
            ->setText('8')
            ->setIsCorrect(true)
            ->setQuestion($question4);
        $answer42 = (new Answer())
            ->setText('4')
            ->setIsCorrect(false)
            ->setQuestion($question4);
        $answer43 = (new Answer())
            ->setText('0')
            ->setIsCorrect(false)
            ->setQuestion($question4);
        $answer44 = (new Answer())
            ->setText('0 + 8')
            ->setIsCorrect(true)
            ->setQuestion($question4);
        $manager->persist($question4);
        $manager->persist($answer41);
        $manager->persist($answer42);
        $manager->persist($answer43);
        $manager->persist($answer44);
        $test->addQuestion($question4);

        $question5 = (new Question())->setText('5 + 5 = ');
        $answer51 = (new Answer())
            ->setText('6')
            ->setIsCorrect(false)
            ->setQuestion($question5);
        $answer52 = (new Answer())
            ->setText('18')
            ->setIsCorrect(false)
            ->setQuestion($question5);
        $answer53 = (new Answer())
            ->setText('10')
            ->setIsCorrect(true)
            ->setQuestion($question5);
        $answer54 = (new Answer())
            ->setText('9')
            ->setIsCorrect(false)
            ->setQuestion($question5);
        $answer55 = (new Answer())
            ->setText('0')
            ->setIsCorrect(false)
            ->setQuestion($question5);
        $manager->persist($question5);
        $manager->persist($answer51);
        $manager->persist($answer52);
        $manager->persist($answer53);
        $manager->persist($answer54);
        $manager->persist($answer55);
        $test->addQuestion($question5);

        $question6 = (new Question())->setText('6 + 6 = ');
        $answer61 = (new Answer())
            ->setText('3')
            ->setIsCorrect(false)
            ->setQuestion($question6);
        $answer62 = (new Answer())
            ->setText('9')
            ->setIsCorrect(false)
            ->setQuestion($question6);
        $answer63 = (new Answer())
            ->setText('0')
            ->setIsCorrect(false)
            ->setQuestion($question6);
        $answer64 = (new Answer())
            ->setText('12')
            ->setIsCorrect(true)
            ->setQuestion($question6);
        $answer65 = (new Answer())
            ->setText('5 + 7')
            ->setIsCorrect(true)
            ->setQuestion($question6);
        $manager->persist($question6);
        $manager->persist($answer61);
        $manager->persist($answer62);
        $manager->persist($answer63);
        $manager->persist($answer64);
        $manager->persist($answer65);
        $test->addQuestion($question6);

        $question7 = (new Question())->setText('7 + 7 = ');
        $answer71 = (new Answer())
            ->setText('5')
            ->setIsCorrect(false)
            ->setQuestion($question7);
        $answer72 = (new Answer())
            ->setText('14')
            ->setIsCorrect(true)
            ->setQuestion($question7);
        $manager->persist($question7);
        $manager->persist($answer71);
        $manager->persist($answer72);
        $test->addQuestion($question7);

        $question8 = (new Question())->setText('8 + 8 = ');
        $answer81 = (new Answer())
            ->setText('16')
            ->setIsCorrect(true)
            ->setQuestion($question8);
        $answer82 = (new Answer())
            ->setText('12')
            ->setIsCorrect(false)
            ->setQuestion($question8);
        $answer83 = (new Answer())
            ->setText('9')
            ->setIsCorrect(false)
            ->setQuestion($question8);
        $answer84 = (new Answer())
            ->setText('5')
            ->setIsCorrect(true)
            ->setQuestion($question8);
        $manager->persist($question8);
        $manager->persist($answer81);
        $manager->persist($answer82);
        $manager->persist($answer83);
        $manager->persist($answer84);
        $test->addQuestion($question8);

        $manager->flush();
    }
}
