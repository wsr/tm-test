<?php

namespace App\Repository;

use App\Entity\TestResult;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TestResult|null find($id, $lockMode = null, $lockVersion = null)
 * @method TestResult|null findOneBy(array $criteria, array $orderBy = null)
 * @method TestResult[] findAll()
 * @method TestResult[] findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TestResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TestResult::class);
    }

    /**
     * @param string $userIdentity
     *
     * @return TestResult[]
     */
    public function findByUserIdentity(string $userIdentity): array
    {
        return $this->findBy(['userIdentity' => $userIdentity], ['startedAt' => Criteria::DESC]);
    }
}
