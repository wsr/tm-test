<?php

namespace App\Entity;

use App\Core\QuestionInterface;
use App\Core\TestInterface;
use App\Repository\TestRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: TestRepository::class)]
class Test implements TestInterface
{
    #[ORM\Id]
    #[ORM\Column(type: "ulid", unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UlidGenerator::class)]
    private Ulid $uuid;

    #[ORM\Column(type: "string")]
    private string $title;

    #[ORM\JoinTable(name: 'test_question')]
    #[ORM\JoinColumn(name: 'test_uuid', referencedColumnName: 'uuid')]
    #[ORM\InverseJoinColumn(name: 'question_uuid', referencedColumnName: 'uuid', unique: true)]
    #[ORM\ManyToMany(targetEntity: Question::class)]
    private Collection $questions;

    #[ORM\OneToMany(mappedBy: 'test', targetEntity: TestResult::class)]
    private Collection $results;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->getTitle();
    }

    /**
     * @return Ulid
     */
    public function getUuid(): Ulid
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param Question $question
     *
     * @return self
     */
    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions->add($question);
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    /**
     * @return Collection
     */
    public function getResults(): Collection
    {
        return $this->results;
    }

    /**
     * @return string
     */
    public function getIdentity(): string
    {
        return $this->getUuid()->toRfc4122();
    }

    /**
     * @return QuestionInterface[]
     */
    public function getQuestionsAsArray(): array
    {
        return $this->getQuestions()->toArray();
    }
}
