<?php

namespace App\Entity;

use App\Core\UserInterface;

class User implements UserInterface
{
    /**
     * @var string
     */
    private string $identity;

    /**
     * @param string $identity
     *
     * @return self
     */
    public function setIdentity(string $identity): self
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentity(): string
    {
        return $this->identity;
    }
}