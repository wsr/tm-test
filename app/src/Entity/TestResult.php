<?php

namespace App\Entity;

use App\Core\TestResultInterface;
use App\Core\UserInterface;
use App\Repository\TestResultRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UlidGenerator;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity(repositoryClass: TestResultRepository::class)]
class TestResult implements TestResultInterface
{
    #[ORM\Id]
    #[ORM\Column(type: "ulid", unique: true)]
    #[ORM\GeneratedValue(strategy: "CUSTOM")]
    #[ORM\CustomIdGenerator(class: UlidGenerator::class)]
    private Ulid $uuid;

    #[ORM\Column(name: 'user_identity', type: "string")]
    private string $userIdentity;

    #[ORM\ManyToOne(targetEntity: Test::class, inversedBy: 'results')]
    #[ORM\JoinColumn(name: 'test_uuid', referencedColumnName: 'uuid')]
    private Test $test;

    #[ORM\Column(type: 'json')]
    private array $questions;

    #[ORM\Column(name: 'started_at', type: "datetime")]
    private \DateTimeInterface $startedAt;

    #[ORM\Column(name: 'ended_at', type: "datetime", nullable: true)]
    private ?\DateTimeInterface $endedAt = null;

    public function __construct()
    {
        $this->startedAt = new \DateTime();
    }

    /**
     * @return Ulid
     */
    public function getUuid(): Ulid
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function getUserIdentity(): string
    {
        return $this->userIdentity;
    }

    /**
     * @param string $userIdentity
     *
     * @return self
     */
    public function setUserIdentity(string $userIdentity): self
    {
        $this->userIdentity = $userIdentity;

        return $this;
    }

    /**
     * @return Test
     */
    public function getTest(): Test
    {
        return $this->test;
    }

    /**
     * @param Test $test
     *
     * @return self
     */
    public function setTest(Test $test): self
    {
        $this->test = $test;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getStartedAt(): \DateTimeInterface
    {
        return $this->startedAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getEndedAt(): ?\DateTimeInterface
    {
        return $this->endedAt;
    }

    /**
     * @param \DateTimeInterface|null $endedAt
     *
     * @return self
     */
    public function setEndedAt(?\DateTimeInterface $endedAt): self
    {
        $this->endedAt = $endedAt;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnded(): bool
    {
        return !empty($this->getEndedAt());
    }

    /**
     * @return array
     */
    public function getQuestions(): array
    {
        return $this->questions;
    }

    /**
     * @param array $questions
     *
     * @return self
     */
    public function setQuestions(array $questions): self
    {
        $this->questions = $questions;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentity(): string
    {
        return $this->getUuid()->toRfc4122();
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(UserInterface $user): self
    {
        return $this->setUserIdentity($user->getIdentity());
    }
}
