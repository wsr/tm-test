<?php

namespace App\Controller;

use App\Core\TestResultInterface;
use App\Entity\TestResult;
use App\Service\TestServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class TestResultController extends AbstractController
{
    #[Route(path: '/test-result/view/{uuid}', name: 'test-result-view')]
    public function view(TestServiceInterface $testService, TestResult $testResult): Response
    {
        if (!$testResult->isEnded()) {
            return $this->redirectToRoute('test-result-process', ['uuid' => $testResult->getIdentity()]);
        }

        return $this->render('test-result/view.twig', [
            'testResult'      => $testResult,
            'questionsResult' => $testService->getQuestionsResult($testResult),
        ]);
    }

    #[Route(path: '/test-result/process/{uuid}/{questionIndex}', name: 'test-result-process')]
    public function process(Request $request, TestServiceInterface $testService, TestResult $testResult, int $questionIndex = null): Response
    {
        if ($testResult->isEnded()) {
            return $this->redirectToRoute('test-result-view', ['uuid' => $testResult->getIdentity()]);
        }

        if ($testService->isAllAnswersDone($testResult)) {
            $testService->finishTest($testResult);

            return $this->redirectToRoute('test-result-finish', ['uuid' => $testResult->getIdentity()]);
        }

        if ($questionIndex === null) {
            $questionIndex = $testService->getFirstQuestionIndex($testResult);
        }

        $question = $testService->getQuestionByIndex($testResult, $questionIndex);
        $answers = $testService->getAnswersByQuestion($testResult, $question);

        /**
         * Сохранение и переход дальше
         */
        if ($request->isMethod(Request::METHOD_POST) && in_array($request->get('action'), ['previous', 'next', 'finish']) && is_array($request->get('answer'))) {
            $selectedAnswers = array_keys(array_filter(array_map(function($answer) { return $answer === 'on'; }, $request->get('answer'))));
            $testService->saveQuestionSelectedAnswersIndexes($testResult, $question, $selectedAnswers);

            switch (true) {
                case $request->get('action') === 'previous' && $testService->hasPreviousQuestion($testResult, $question):
                    return $this->redirectToRoute('test-result-process', [
                        'uuid'          => $testResult->getIdentity(),
                        'questionIndex' => $testService->getPreviousQuestionIndex($testResult, $questionIndex),
                    ]);
                case $request->get('action') === 'next' && $testService->hasNextQuestion($testResult, $question):
                    return $this->redirectToRoute('test-result-process', [
                        'uuid'          => $testResult->getIdentity(),
                        'questionIndex' => $testService->getNextQuestionIndex($testResult, $questionIndex),
                    ]);
                case $request->get('action') === 'finish' && !$testService->hasNextQuestion($testResult, $question):
                    if ($testService->isAllAnswersDone($testResult)) {
                        $testService->finishTest($testResult);

                        return $this->redirectToRoute('test-result-finish', ['uuid' => $testResult->getIdentity()]);
                    }
                    
                    return $this->redirectToRoute('test-result-process', ['uuid' => $testResult->getIdentity()]);
            }
        }

        $selectedAnswers = $testService->getSelectedAnswerIdentities($testResult, $question);

        return $this->render('test-result/process.twig', [
            'testResult'          => $testResult,
            'questionsCount'      => $testService->getQuestionsCount($testResult),
            'questionIndex'       => $questionIndex,
            'question'            => $question,
            'answers'             => $answers,
            'selectedAnswers'     => $selectedAnswers,
            'hasNextQuestion'     => $testService->hasNextQuestion($testResult, $question),
            'hasPreviousQuestion' => $testService->hasPreviousQuestion($testResult, $question),
        ]);
    }

    #[Route(path: '/test-result/finish/{uuid}', name: 'test-result-finish')]
    public function finish(TestServiceInterface $testService, TestResult $testResult): Response
    {
        if (!$testService->isAllAnswersDone($testResult)) {
            return $this->redirectToRoute('test-result-process', ['uuid' => $testResult->getIdentity()]);
        }

        return $this->render('test-result/finish.twig', [
            'testResult' => $testResult,
        ]);
    }
}