<?php

namespace App\Controller;

use App\Entity\Test;
use App\Service\TestServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class TestController extends AbstractController
{
    #[Route(path: '/test/run/{uuid}', name: 'test-run')]
    public function run(TestServiceInterface $testService, Test $test): Response
    {
        $testResult = $testService->startTest($test);

        return $this->redirectToRoute('test-result-process', ['uuid' => $testResult->getUuid()->toRfc4122()]);
    }
}