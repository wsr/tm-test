<?php

namespace App\Controller;

use App\Service\TestServiceInterface;
use App\Service\UserServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class MainController extends AbstractController
{
    #[Route(path: '/', name: 'index')]
    public function index(UserServiceInterface $userService, TestServiceInterface $testService): Response
    {
        $tests = $testService->getTests();
        $testResults = $testService->getUserTestResults($userService->getUser());

        return $this->render('main/index.twig', [
            'user'        => $userService->getUser(),
            'tests'       => $tests,
            'testResults' => $testResults,
        ]);
    }
}