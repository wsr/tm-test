<?php

namespace App\Service;

use App\Core\AnswerInterface;
use App\Core\QuestionInterface;
use App\Core\TestInterface;
use App\Core\TestResultInterface;
use App\Core\UserInterface;

interface TestServiceInterface
{
    /**
     * @param TestInterface $test
     *
     * @return TestResultInterface
     */
    public function startTest(TestInterface $test): TestResultInterface;

    /**
     * @return TestInterface[]
     */
    public function getTests(): array;

    /**
     * @param UserInterface $user
     *
     * @return TestResultInterface[]
     */
    public function getUserTestResults(UserInterface $user): array;

    /**
     * @param TestResultInterface $testResult
     *
     * @return QuestionInterface
     */
    public function getFirstQuestion(TestResultInterface $testResult): QuestionInterface;

    /**
     * @param TestResultInterface $testResult
     * @param string              $questionIdentity
     *
     * @return QuestionInterface|null
     */
    public function getQuestionByIdentity(TestResultInterface $testResult, string $questionIdentity): ?QuestionInterface;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     *
     * @return AnswerInterface[]
     */
    public function getAnswersByQuestion(TestResultInterface $testResult, QuestionInterface $question): array;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     *
     * @return string[]
     */
    public function getSelectedAnswerIdentities(TestResultInterface $testResult, QuestionInterface $question): array;

    /**
     * @param TestResultInterface $testResult
     *
     * @return int
     */
    public function getQuestionsCount(TestResultInterface $testResult): int;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     *
     * @return QuestionInterface|null
     */
    public function getNextQuestion(TestResultInterface $testResult, QuestionInterface $question): ?QuestionInterface;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     *
     * @return QuestionInterface|null
     */
    public function getPreviousQuestion(TestResultInterface $testResult, QuestionInterface $question): ?QuestionInterface;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     *
     * @return bool
     */
    public function hasNextQuestion(TestResultInterface $testResult, QuestionInterface $question): bool;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     *
     * @return bool
     */
    public function hasPreviousQuestion(TestResultInterface $testResult, QuestionInterface $question): bool;

    /**
     * @param TestResultInterface $testResult
     *
     * @return bool
     */
    public function isAllAnswersDone(TestResultInterface $testResult): bool;

    /**
     * @param TestResultInterface $testResult
     * @param QuestionInterface   $question
     * @param string[]            $selectedAnswerIdentities
     *
     * @return void
     */
    public function saveQuestionSelectedAnswers(TestResultInterface $testResult, QuestionInterface $question, array $selectedAnswerIdentities): void;

    /**
     * @param TestResultInterface $testResult
     *
     * @return void
     */
    public function finishTest(TestResultInterface $testResult): void;

    /**
     * @param TestResultInterface $testResult
     *
     * @return array
     */
    public function getQuestionsResult(TestResultInterface $testResult): array;
}