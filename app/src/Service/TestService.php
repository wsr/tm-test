<?php

namespace App\Service;

use App\Core\QuestionInterface;
use App\Core\TestInterface;
use App\Core\TestResultInterface;
use App\Core\UserInterface;
use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Test;
use App\Entity\TestResult;
use App\Exception\NotFinishedTestException;
use App\Exception\NotFoundAnswersException;
use App\Exception\NotFoundQuestionException;
use App\Exception\NotFoundQuestionIndexException;
use App\Repository\AnswerRepository;
use App\Repository\QuestionRepository;
use App\Repository\TestRepository;
use App\Repository\TestResultRepository;
use Doctrine\ORM\EntityManagerInterface;

readonly class TestService implements TestServiceInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private UserServiceInterface $userService,
    ){}

    /**
     * @param Test $test
     *
     * @return TestResult
     */
    public function startTest(TestInterface $test): TestResultInterface
    {
        $testResult = (new TestResult())
            ->setUser($this->userService->getUser())
            ->setTest($test)
            ->setQuestions($this->prepareQuestions($test))
        ;

        $this->entityManager->persist($testResult);
        $this->entityManager->flush();

        return $testResult;
    }

    /**
     * @return Test[]
     */
    public function getTests(): array
    {
        /** @var TestRepository $testRepository */
        $testRepository = $this->entityManager->getRepository(Test::class);

        return $testRepository->findAll();
    }

    /**
     * @return TestResult[]
     */
    public function getUserTestResults(UserInterface $user): array
    {
        /** @var TestResultRepository $testResultRepository */
        $testResultRepository = $this->entityManager->getRepository(TestResult::class);

        return $testResultRepository->findByUserIdentity($user->getIdentity());
    }

    /**
     * @param Test $test
     *
     * @return array
     */
    private function prepareQuestions(Test $test): array
    {
        $questions = $test->getQuestionsAsArray();
        shuffle($questions);

        $result = [];
        foreach ($questions as $question) {
            $data    = [
                'question' => $question->getIdentity(),
                'answers'  => [],
            ];
            $answers = $question->getAnswersAsArray();
            shuffle($answers);

            foreach ($answers as $answer) {
                $data['answers'][] = [
                    'answer'   => $answer->getIdentity(),
                    'selected' => false,
                ];
            }

            $result[] = $data;
        }

        return $result;
    }

    /**
     * @param TestResult $testResult
     *
     * @return Question
     *
     * @throws NotFoundQuestionException
     */
    public function getFirstQuestion(TestResultInterface $testResult): QuestionInterface
    {
        $index = $this->getFirstQuestionIndex($testResult);

        return $this->getQuestionByIndex($testResult, $index);
    }

    /**
     * @param TestResult $testResult
     *
     * @return int
     */
    public function getFirstQuestionIndex(TestResultInterface $testResult): int
    {
        foreach ($testResult->getQuestions() as $questionIndex => $question) {
            $hasSelectedAnswers = false;
            foreach ($question['answers'] as $answer) {
                if ($answer['selected']) {
                    $hasSelectedAnswers = true;
                }
            }
            if (!$hasSelectedAnswers) {
                return $questionIndex;
            }
        }

        return 0;
    }

    /**
     * @param TestResult $testResult
     * @param int        $questionIndex
     *
     * @return Question
     *
     * @throws NotFoundQuestionException
     */
    public function getQuestionByIndex(TestResultInterface $testResult, int $questionIndex): QuestionInterface
    {
        $questionInfo = $testResult->getQuestions()[$questionIndex] ?? null;
        if ($questionInfo === null) {
            throw new NotFoundQuestionException();
        }

        return $this->getQuestionByIdentity($testResult, $questionInfo['question']);
    }

    /**
     * @param TestResult $testResult
     * @param string     $questionIdentity
     *
     * @return Question
     *
     * @throws NotFoundQuestionException
     */
    public function getQuestionByIdentity(TestResultInterface $testResult, string $questionIdentity): QuestionInterface
    {
        /** @var QuestionRepository $questionRepository */
        $questionRepository = $this->entityManager->getRepository(Question::class);
        $question = $questionRepository->find($questionIdentity);
        if (!$question) {
            throw new NotFoundQuestionException();
        }

        return $question;
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return Answer[]
     *
     * @throws NotFoundAnswersException
     */
    public function getAnswersByQuestion(TestResultInterface $testResult, QuestionInterface $question): array
    {
        $found = false;
        $answerIdentities = [];
        foreach ($testResult->getQuestions() as $questionInfo) {
            if ($questionInfo['question'] === $question->getIdentity()) {
                $found = true;
                $answerIdentities = array_map(function($answerInfo){
                    return $answerInfo['answer'];
                }, $questionInfo['answers']);

                break;
            }
        }

        if (!$found) {
            throw new NotFoundAnswersException();
        }

        /** @var AnswerRepository $answerRepository */
        $answerRepository = $this->entityManager->getRepository(Answer::class);
        $answers = $answerRepository->findBy(['uuid' => $answerIdentities]);

        if (sizeof($answers) !== sizeof($answerIdentities)) {
            throw new NotFoundAnswersException();
        }

        $result = [];
        foreach ($answerIdentities as $answerIdentity) {
            foreach ($answers as $answer) {
                if ($answerIdentity === $answer->getIdentity()) {
                    $result[] = $answer;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return string[]
     */
    public function getSelectedAnswerIdentities(TestResultInterface $testResult, QuestionInterface $question): array
    {
        foreach ($testResult->getQuestions() as $questionInfo) {
            if ($questionInfo['question'] === $question->getIdentity()) {
                return array_values(array_map(function($answerInfo) {
                    return $answerInfo['answer'];
                }, array_filter($questionInfo['answers'], function($answerInfo) {
                    return $answerInfo['selected'];
                })));
            }
        }

        return [];
    }

    /**
     * @param TestResult $testResult
     *
     * @return int
     */
    public function getQuestionsCount(TestResultInterface $testResult): int
    {
        return sizeof($testResult->getQuestions());
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return int
     *
     * @throws NotFoundQuestionIndexException
     */
    private function getIndexByQuestion(TestResultInterface $testResult, QuestionInterface $question): int
    {
        foreach ($testResult->getQuestions() as $questionIndex => $questionInfo) {
            if ($questionInfo['question'] === $question->getIdentity()) {
                return $questionIndex;
            }
        }

        throw new NotFoundQuestionIndexException();
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return Question|null
     *
     * @throws NotFoundQuestionException
     * @throws NotFoundQuestionIndexException
     */
    public function getNextQuestion(TestResultInterface $testResult, QuestionInterface $question): ?QuestionInterface
    {
        $currentQuestionIndex = $this->getIndexByQuestion($testResult, $question);
        $nextQuestionIndex = $this->getNextQuestionIndex($testResult, $currentQuestionIndex);
        if ($nextQuestionIndex !== null) {
            return $this->getQuestionByIndex($testResult, $nextQuestionIndex);
        }

        return null;
    }

    /**
     * @param TestResult $testResult
     * @param int        $currentQuestionIndex
     *
     * @return int|null
     */
    public function getNextQuestionIndex(TestResultInterface $testResult, int $currentQuestionIndex): ?int
    {
        $questionsCount = $this->getQuestionsCount($testResult);
        if ($currentQuestionIndex < $questionsCount - 1) {
            return $currentQuestionIndex + 1;
        }

        return null;
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return Question|null
     *
     * @throws NotFoundQuestionException
     * @throws NotFoundQuestionIndexException
     */
    public function getPreviousQuestion(TestResultInterface $testResult, QuestionInterface $question): ?QuestionInterface
    {
        $currentQuestionIndex = $this->getIndexByQuestion($testResult, $question);
        $previousQuestionIndex = $this->getPreviousQuestionIndex($testResult, $currentQuestionIndex);
        if ($previousQuestionIndex !== null) {
            return $this->getQuestionByIndex($testResult, $previousQuestionIndex);
        }

        return null;
    }

    /**
     * @param TestResult $testResult
     * @param int        $currentQuestionIndex
     *
     * @return int|null
     */
    public function getPreviousQuestionIndex(TestResultInterface $testResult, int $currentQuestionIndex): ?int
    {
        if ($currentQuestionIndex > 0) {
            return $currentQuestionIndex - 1;
        }

        return null;
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return bool
     *
     * @throws NotFoundQuestionIndexException
     */
    public function hasNextQuestion(TestResultInterface $testResult, QuestionInterface $question): bool
    {
        $currentQuestionIndex = $this->getIndexByQuestion($testResult, $question);
        $nextQuestionIndex = $this->getNextQuestionIndex($testResult, $currentQuestionIndex);

        return $nextQuestionIndex !== null;
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     *
     * @return bool
     *
     * @throws NotFoundQuestionIndexException
     */
    public function hasPreviousQuestion(TestResultInterface $testResult, QuestionInterface $question): bool
    {
        $currentQuestionIndex = $this->getIndexByQuestion($testResult, $question);
        $previousQuestionIndex = $this->getPreviousQuestionIndex($testResult, $currentQuestionIndex);

        return $previousQuestionIndex !== null;
    }

    /**
     * @param TestResult $testResult
     *
     * @return bool
     */
    public function isAllAnswersDone(TestResultInterface $testResult): bool
    {
        foreach ($testResult->getQuestions() as $questionInfo) {
            $hasSelectedAnswers = false;
            foreach ($questionInfo['answers'] as $answerInfo) {
                if ($answerInfo['selected']) {
                    $hasSelectedAnswers = true;
                }
            }

            if (!$hasSelectedAnswers) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     * @param array      $selectedAnswerIndexes
     *
     * @return void
     */
    public function saveQuestionSelectedAnswersIndexes(TestResultInterface $testResult, QuestionInterface $question, array $selectedAnswerIndexes): void
    {
        $selectedAnswerIdentities = [];
        foreach ($testResult->getQuestions() as $questionInfo) {
            if ($questionInfo['question'] !== $question->getIdentity()) {
                continue;
            }

            $answersInfo = $questionInfo['answers'];
            foreach ($answersInfo as $answerIndex => $answerInfo) {
                if (in_array($answerIndex, $selectedAnswerIndexes)) {
                    $selectedAnswerIdentities[] = $answerInfo['answer'];
                }
            }
        }

        $this->saveQuestionSelectedAnswers($testResult, $question, $selectedAnswerIdentities);
    }

    /**
     * @param TestResult $testResult
     * @param Question   $question
     * @param string[]   $selectedAnswerIdentities
     *
     * @return void
     */
    public function saveQuestionSelectedAnswers(TestResultInterface $testResult, QuestionInterface $question, array $selectedAnswerIdentities): void
    {
        $questions = $testResult->getQuestions();
        foreach ($questions as $questionIndex => $questionInfo) {
            if ($questionInfo['question'] !== $question->getIdentity()) {
                continue;
            }

            $answersInfo = $questionInfo['answers'];
            foreach ($answersInfo as $answerIndex => $answerInfo) {
                $answersInfo[$answerIndex]['selected'] = in_array($answerInfo['answer'], $selectedAnswerIdentities);
            }

            $questions[$questionIndex]['answers'] = $answersInfo;
        }
        $testResult->setQuestions($questions);

        $this->entityManager->flush();
    }

    /**
     * @param TestResult $testResult
     *
     * @return void
     *
     * @throws NotFinishedTestException
     */
    public function finishTest(TestResultInterface $testResult): void
    {
        if (!$this->isAllAnswersDone($testResult)) {
            throw new NotFinishedTestException();
        }

        $testResult->setEndedAt(new \DateTime());

        $this->entityManager->flush();
    }

    /**
     * @param TestResult $testResult
     *
     * @return array
     *
     * @throws NotFoundAnswersException
     * @throws NotFoundQuestionException
     */
    public function getQuestionsResult(TestResultInterface $testResult): array
    {
        $result = [];
        foreach ($testResult->getQuestions() as $questionIndex => $questionInfo) {
            $question = $this->getQuestionByIndex($testResult, $questionIndex);
            $answers = $this->getAnswersByQuestion($testResult, $question);
            $selectedAnswers = $this->getSelectedAnswerIdentities($testResult, $question);

            $isCorrect = true;
            foreach ($answers as $answer) {
                if (!$answer->getIsCorrect() && in_array($answer->getIdentity(), $selectedAnswers)) {
                    $isCorrect = false;
                    break;
                }
            }

            $result[] = [
                'question'        => $question,
                'answers'         => $answers,
                'selectedAnswers' => $selectedAnswers,
                'isCorrect'       => $isCorrect,
            ];
        }

        return $result;
    }
}