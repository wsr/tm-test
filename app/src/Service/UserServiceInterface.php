<?php

namespace App\Service;

use App\Core\UserInterface;

interface UserServiceInterface
{
    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface;
}