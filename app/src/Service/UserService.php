<?php

namespace App\Service;

use App\Core\UserInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;

readonly class UserService implements UserServiceInterface
{
    /**
     * @param RequestStack $requestStack
     */
    public function __construct(private RequestStack $requestStack)
    {
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return (new User())->setIdentity($this->requestStack->getSession()->getId());
    }
}