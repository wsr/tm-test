<?php

namespace App\Tests\Service;

use App\Core\TestInterface;
use App\Entity\Test;
use App\Repository\TestRepository;
use App\Service\TestService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TestServiceTest extends KernelTestCase
{
    public function testGetTests()
    {
        self::bootKernel();
        $container = static::getContainer();

        $testRepository = $this->createMock(TestRepository::class);
        $testRepository->expects(self::once())
           ->method('findAll')
           ->willReturn([
               new Test(),
               new Test(),
               new Test(),
           ])
        ;

        $container->set(TestRepository::class, $testRepository);

        /** @var TestService $testService */
        $testService = $container->get(TestService::class);

        $tests = $testService->getTests();
        $this->assertCount(3, $tests, 'Tests count not 3');
        $this->assertContainsOnlyInstancesOf(TestInterface::class, $tests, 'TestInterface::class objects only');
    }
}
