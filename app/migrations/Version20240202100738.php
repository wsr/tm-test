<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240202100738 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE answer (uuid UUID NOT NULL, question_uuid UUID DEFAULT NULL, text VARCHAR(255) NOT NULL, is_correct BOOLEAN NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('CREATE INDEX IDX_DADD4A258F91BD49 ON answer (question_uuid)');
        $this->addSql('COMMENT ON COLUMN answer.uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN answer.question_uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('CREATE TABLE question (uuid UUID NOT NULL, text VARCHAR(255) NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN question.uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('CREATE TABLE test (uuid UUID NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(uuid))');
        $this->addSql('COMMENT ON COLUMN test.uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('CREATE TABLE test_question (test_uuid UUID NOT NULL, question_uuid UUID NOT NULL, PRIMARY KEY(test_uuid, question_uuid))');
        $this->addSql('CREATE INDEX IDX_23944218D4067A69 ON test_question (test_uuid)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_239442188F91BD49 ON test_question (question_uuid)');
        $this->addSql('COMMENT ON COLUMN test_question.test_uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN test_question.question_uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('CREATE TABLE test_result (uuid UUID NOT NULL, test_uuid UUID DEFAULT NULL, user_identity VARCHAR(255) NOT NULL, questions JSON NOT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ended_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(uuid))');
        $this->addSql('CREATE INDEX IDX_84B3C63DD4067A69 ON test_result (test_uuid)');
        $this->addSql('COMMENT ON COLUMN test_result.uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN test_result.test_uuid IS \'(DC2Type:ulid)\'');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A258F91BD49 FOREIGN KEY (question_uuid) REFERENCES question (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_question ADD CONSTRAINT FK_23944218D4067A69 FOREIGN KEY (test_uuid) REFERENCES test (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_question ADD CONSTRAINT FK_239442188F91BD49 FOREIGN KEY (question_uuid) REFERENCES question (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE test_result ADD CONSTRAINT FK_84B3C63DD4067A69 FOREIGN KEY (test_uuid) REFERENCES test (uuid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE answer DROP CONSTRAINT FK_DADD4A258F91BD49');
        $this->addSql('ALTER TABLE test_question DROP CONSTRAINT FK_23944218D4067A69');
        $this->addSql('ALTER TABLE test_question DROP CONSTRAINT FK_239442188F91BD49');
        $this->addSql('ALTER TABLE test_result DROP CONSTRAINT FK_84B3C63DD4067A69');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE test');
        $this->addSql('DROP TABLE test_question');
        $this->addSql('DROP TABLE test_result');
    }
}
